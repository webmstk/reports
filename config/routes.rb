Rails.application.routes.draw do
  devise_for :users

  root 'reports#index'

  resources :reports, shallow: true, only: [:index, :show, :create] do
    get :send_report, on: :member
    resources :tasks, only: [:create, :destroy]
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
