# == Schema Information
#
# Table name: tasks
#
#  id             :integer          not null, primary key
#  description    :text
#  time_estimated :integer
#  time_wasted    :integer
#  status         :integer
#  user_id        :integer
#  report_id      :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
# Indexes
#
#  index_tasks_on_report_id  (report_id)
#  index_tasks_on_user_id    (user_id)
#

class Task < ApplicationRecord
  BACKLOG_STATUS = 0
  TODO_STATUS = 1
  DONE_STATUS = 2

  STATUSES = [
    BACKLOG_STATUS,
    TODO_STATUS,
    DONE_STATUS
  ].freeze

  belongs_to :user
  belongs_to :report, optional: true

  validates :description, :status, presence: true
  validates :status, inclusion: { in: STATUSES }

  scope :done, -> { where status: DONE_STATUS }
  scope :todo, -> { where status: TODO_STATUS }
  scope :backlog, -> { where status: BACKLOG_STATUS }
  scope :reversed, -> { order(id: :desc) }

end
