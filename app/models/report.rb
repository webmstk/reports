# == Schema Information
#
# Table name: reports
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_reports_on_user_id  (user_id)
#

class Report < ApplicationRecord
  belongs_to :user
  has_many :tasks, dependent: :destroy

  scope :reversed, -> { order id: :desc }

  def report_tasks
    Task.where(user_id: user_id, status: Task::BACKLOG_STATUS)
        .or Task.where(report: self)
  end
end
