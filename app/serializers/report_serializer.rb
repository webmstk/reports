class ReportSerializer < Grape::Entity
  expose :tasks, merge: true, using: TaskSerializer do |report|
    report.report_tasks.reversed
  end
end
