class TaskSerializer < Grape::Entity
  expose :id, :description, :time_wasted, :status, :user_id
end
