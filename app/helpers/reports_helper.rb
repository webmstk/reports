module ReportsHelper
  def report_name(report)
    "Отчёт от #{format_date report.created_at} / #{report.user&.email}"
  end
end
