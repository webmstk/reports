module ApplicationHelper
  def format_date(date)
    Russian::strftime(date, "%d %B %Y")
  end
end
