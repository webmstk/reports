import Vue from 'vue'
import VueResource from 'vue-resource'

Vue.use(VueResource)

const customActions = {
  save_task: { method: 'POST', url: '/reports{/report_id}/tasks' },
  send_report: { method: 'GET', url: '/reports{/report_id}/send_report' },
  delete_task: { method: 'DELETE', url: '/tasks{/task_id}' }
}

export default Vue.resource('/reports{/report_id}', { format: 'json' }, customActions)
