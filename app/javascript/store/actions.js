import http from '../components/reports/http'

export const fetch_report = ({ commit }, report_id) => (
  http.get({ report_id }).then(response => {
      return response.json()
    })
    .then(function (data) {
      commit('load_tasks', data.tasks)
      commit('set_current_user', data.current_user_id)
      commit('set_sendable')
    })
)

export const create_task = ({ commit }, { report_id, task }) => {
  if (!task.description) {
    alert('Попробуйте ещё раз, когда будет что написать :)')
    return false
  }

  return http.save_task({ report_id }, { task })
    .then(response => {
      return response.json()
    })
    .then(task => {
      commit('add_task', task)
      commit('set_sendable')
    })
}

export const delete_task = ({ commit }, task_id) => (
  http.delete_task({ task_id }).then(response => {
    commit('remove_task', task_id)
    commit('set_sendable')
  })
)

export const send_report = ({ commit }, report_id) => (
  http.send_report({ report_id }).then(response => {
    commit('deny_send')
  })
)
