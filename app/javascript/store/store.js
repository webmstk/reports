import Vue from 'vue'
import Vuex from 'vuex'
import * as actions from './actions'
import { BACKLOG_STATUS, TODO_STATUS, DONE_STATUS } from '../components/reports/constants'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    sendable: false,
    tasks: [],
    backlog_status: BACKLOG_STATUS,
    todo_status: TODO_STATUS,
    done_status: DONE_STATUS,
    current_user_id: null
  },

  getters: {
    tasks_done: state => {
        return state.tasks.filter(task => task.status == DONE_STATUS)
    },
    tasks_todo: state => {
        return state.tasks.filter(task => task.status == TODO_STATUS)
    },
    tasks_backlog: state => {
        return state.tasks.filter(task => task.status == BACKLOG_STATUS)
    },
  },

  mutations: {
    allow_send: state => {
      state.sendable = true
    },
    deny_send: state => {
      state.sendable = false
    },
    load_tasks: (state, tasks) => {
      state.tasks = tasks
      state.loading = false
    },
    set_current_user: (state, user_id) => {
      state.current_user_id = user_id
    },
    add_task: (state, task) => {
      state.tasks = [task, ...state.tasks]
    },
    remove_task: (state, id) => {
      state.tasks = state.tasks.filter(task => task.id != id)
    },
    set_sendable: state => {
      state.sendable =  state.tasks.some(task => task.status != BACKLOG_STATUS)
    }
  },

  actions

})
