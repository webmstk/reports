import Vue from 'vue'
import Reports from '../components/reports/reports.vue'
import VueRouter from 'vue-router'

import { store } from '../store/store.js'

Vue.use(VueRouter)

const router = new VueRouter({
  routes: [{ path: '/reports/:id', component: Reports }],
  mode: 'history'
})

document.addEventListener('DOMContentLoaded', () => {
  document.getElementById('content')
          .appendChild(document.createElement('reports'))

  new Vue({
    el: 'reports',
    router,
    store,
    render: h => h(Reports)
  })
})
