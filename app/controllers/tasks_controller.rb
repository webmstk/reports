class TasksController < ApplicationController
  def create
    report = Report.find params[:report_id]
    task = report.tasks.new(task_params)
    task.user_id = current_user.id

    task.save!
    render json: TaskSerializer.represent(task)
  end

  def destroy
    task = Task.find params[:id]
    authorize task
    head :ok if task.destroy!
  end

  private

  def task_params
    params.require(:task).permit(
      :description,
      :time_wasted,
      :status
    )
  end
end
