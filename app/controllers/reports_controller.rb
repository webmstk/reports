class ReportsController < ApplicationController
  before_action :set_report, only: [:send_report]

  def index
    @reports = Report.reversed.limit(20)
  end

  def show
    respond_to do |format|
      format.html
      format.json do
        report = Report.find params[:id]
        render json: { current_user_id: current_user.id,
                       tasks: ReportSerializer.represent(report) }
      end
    end
  end

  def create
    report = current_user.reports.find_by('created_at >= ?',
                                          Time.zone.now.beginning_of_day)
    report = current_user.reports.create! unless report
    redirect_to report_path(report)
  end

  def send_report
    ReportMailer.send_report(@report).deliver_now
    head :ok
  end

  private

  def set_report
    @report = Report.find params[:id]
  end
end
