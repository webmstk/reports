class ApplicationController < ActionController::Base
  include Pundit

  before_action :authenticate_user!
  protect_from_forgery unless: -> { request.format.json? }
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  private

  def user_not_authorized
    flash[:alert] = 'У вас нет прав на данное действие.'
    redirect_to(request.referrer || root_path)
  end
end
