class ReportMailer < ApplicationMailer
  include ApplicationHelper
  include ReportsHelper

  def send_report(report)
    @tasks = report.tasks
    return if @tasks.empty?

    mail(
      to: report.user.email,
      subject: report_name(report)
    )
  end
end
