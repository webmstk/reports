class CreateTasks < ActiveRecord::Migration[5.1]
  def change
    create_table :tasks do |t|
      t.text :description
      t.integer :time_estimated
      t.integer :time_wasted
      t.integer :status
      t.references :user, foreign_key: true
      t.references :report, foreign_key: true

      t.timestamps
    end
  end
end
