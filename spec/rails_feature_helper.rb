require 'rails_helper'
require 'capybara/rails'
require 'capybara/rspec'

RSpec.configure do |config|
  config.include FeatureMacros, type: :feature
end
