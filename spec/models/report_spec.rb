require 'rails_helper'

describe Report, type: :model do
  describe '#report_tasks' do
    let(:user) { create :user }
    let(:another_user) { create :user }
    let(:report) { create :report, user: user }
    let(:other_report) { create :report, user: user }

    let(:todo_task) { create :task, report: report, status: Task::TODO_STATUS }
    let(:done_task) { create :task, report: report, status: Task::DONE_STATUS }
    let(:other_report_task) { create :task, report: other_report }

    let(:backlog_task) do
      create :task, report_id: nil,
                    user: user,
                    status: Task::BACKLOG_STATUS
    end

    let(:other_user_backlog_task) do
      create :task, report_id: nil,
                    user: another_user,
                    status: Task::BACKLOG_STATUS
    end

    it 'returns todo and done tasks' do
      expect(report.report_tasks).to include todo_task
      expect(report.report_tasks).to include done_task
    end

    it 'does not return task of another report' do
      expect(report.report_tasks).to_not include other_report_task
    end

    it 'returns backlog task of report\'s user' do
      expect(report.report_tasks).to include backlog_task
    end

    it 'does not return backlog task of another user' do
      expect(report.report_tasks).to_not include other_user_backlog_task
    end
  end
end
