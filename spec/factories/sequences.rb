FactoryGirl.define do
  sequence :email do |n|
    "test#{n}@mail.ru"
  end
end
