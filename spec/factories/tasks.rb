FactoryGirl.define do
  factory :task do
    description 'Запилены тесты'
    status Task::TODO_STATUS
    user
  end
end
