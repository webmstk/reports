require 'rails_feature_helper'

feature :auth do
  given(:user) { create :user }

  scenario 'registered user signs in' do
    sign_in user
    expect(page).to have_content 'Вход в систему выполнен.'
    expect(current_path).to eq root_path
  end
end
