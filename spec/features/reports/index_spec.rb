require 'rails_feature_helper'

feature :index do
  given(:user) { create :user }
  given!(:report) { create :report, user: user, created_at: Date.new(2017, 6, 3) }

  scenario 'user sees list of reports' do
    sign_in user
    visit reports_path

    expect(page).to have_content "Отчёт от 03 июня 2017 / #{user.email}"
    expect(page).to have_link 'Новый отчёт'
  end
end
